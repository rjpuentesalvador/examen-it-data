import { ProducComponent } from './produc/produc.component';
import { GaleryComponent } from './galery/galery.component';
import { ContacComponent } from './contac/contac.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'contac',
    component: ContacComponent
  },
  {
    path: 'galery',
    component: GaleryComponent
  },
  {
    path: 'produc',
    component: ProducComponent
  }

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
