import { UsuarioService } from './servicio/usuario.service';
import { ToasterService } from 'angular2-toaster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, TemplateRef, QueryList, ViewChildren } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  modalRef: any;
  idPersona = '';
  title = 'Agregar';
  personal: any = { id: 0, vcCodigo: '', vcNombre: '', vcApellido: '', iEdad: 0 };
  personales: ITPersonal[] = [];
  pagina: any[] = [];
  editar = false;


  // paginacion
  nuevoExamen: any[];
  @ViewChildren('pages') pages: QueryList<any>;
  itemsPpg: number;
  nofvPaginators: number;
  nufPaginators: number;
  paginators: Array<any> = [];
  activePage: number;
  fvIndex: number;
  lvIndex: number;
  fvPaginator: number;
  lvPaginator: number;
  constructor(
    private modal: NgbModal,
    private toasterService: ToasterService,
    private usuarioService: UsuarioService
  ) { }

  ngOnInit() {

  }
  onbuscarPg() {
    this.pagina = this.personales;
    if (this.itemsPpg == undefined) {
      this.itemsPpg = 5;
      this.actulizarPage(this.pagina, this.itemsPpg);
    } else {
      this.actulizarPage(this.pagina, this.itemsPpg);
    }
  }
  onServicio() {
    this.usuarioService.listaUserHttp().then((data) => {
      console.log('data http', data);
    });
  }
  onServicio1() {
    this.usuarioService.listaUserHttpClient().subscribe((data) => {
      this.personales = data;
      console.log('data httpClient', this.personales);
    });
  }
  onGuardar(val) {
    if (this.onValidar()) {
      if (val == 'G') {
        if (this.personales.length > 0) {
          let values;
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < this.personales.length; i++) {
            if (this.personales[i].vcCodigo == this.personal.vcCodigo) {
              values = this.personales[i].vcCodigo;
            }
          }

          if (values === undefined || values == '' || values === 0) {
            this.personales.push({
              vcCodigo: this.personal.vcCodigo,
              vcNombre: this.personal.vcNombre,
              vcApellido: this.personal.vcApellido,
              iEdad: this.personal.iEdad,
            });
            let id = 0;
            // tslint:disable-next-line:prefer-for-of
            for (let i = 0; i < this.personales.length; i++) {
              this.personales[i].id = ++id;
            }
            this.pagina = this.personales;
            if (this.itemsPpg == undefined) {
              this.itemsPpg = 5;
              this.actulizarPage(this.pagina, this.itemsPpg);
            } else {
              this.actulizarPage(this.pagina, this.itemsPpg);
            }
          } else {
            // tslint:disable-next-line:prefer-for-of
            alert('Este personal ya Existe en el Registro');
            this.limpiar();
          }
        } else {
          this.personales.push({
            vcCodigo: this.personal.vcCodigo,
            vcNombre: this.personal.vcNombre,
            vcApellido: this.personal.vcApellido,
            iEdad: this.personal.iEdad,
          });
          let id = 0;
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < this.personales.length; i++) {
            this.personales[i].id = ++id;
          }
          this.pagina = this.personales;
          if (this.itemsPpg == undefined) {
            this.itemsPpg = 5;
            this.actulizarPage(this.pagina, this.itemsPpg);
          } else {
            this.actulizarPage(this.pagina, this.itemsPpg);
          }
        }
      } else if (val == 'A') {
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < this.personales.length; i++) {
          if (this.personales[i].vcCodigo == this.personal.vcCodigo) {
            this.personales[i].vcNombre = this.personal.vcNombre;
            this.personales[i].vcApellido = this.personal.vcApellido;
            this.personales[i].iEdad = this.personal.iEdad;
          }
        }
      }

      this.limpiar();
      this.editar = false;
      this.title = 'Nuevo'
    }
  }
  onCancelar() {
    this.limpiar();
    this.editar = false;
    this.title = 'Nuevo';
  }
  limpiar() {
    this.personal.vcCodigo = '';
    this.personal.vcCodigo = '';
    this.personal.vcNombre = '';
    this.personal.vcApellido = '';
    this.personal.iEdad = 0;
  }
  onEditar(data) {
    this.title = 'Editar';
    this.editar = true;
    this.personal.vcCodigo = data.vcCodigo;
    this.personal.vcNombre = data.vcNombre;
    this.personal.vcApellido = data.vcApellido;
    this.personal.iEdad = data.iEdad;
  }
  onEliminar(data, alert) {
    this.idPersona = data.vcCodigo;
    this.openModalsm(alert);
  }
  onConfirmarSi() {
    this.eliminar(this.personales, this.idPersona);
    this.cerrar(this.idPersona);
    this.idPersona = '';
    let id = 0;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.personales.length; i++) {
      this.personales[i].id = ++id;
    }
  }
  onConfirmarNo() {
    this.descartar();
  }

  onValidar(): boolean {
    if (this.personal.vcCodigo == null || this.personal.vcCodigo == '' || this.personal.vcCodigo == undefined) {
      // this.toasterService.pop('error', 'Error', 'Debe agregar código');
      alert('Debe agregar código');
      return;
    } else if (this.personal.vcNombre == null || this.personal.vcNombre == '' || this.personal.vcNombre == undefined) {
      // this.toasterService.pop('error', 'Error', 'Debe agregar  nombre');
      alert('Debe agregar nombre');

      return;
    } else if (this.personal.vcApellido == null || this.personal.vcApellido == '' || this.personal.vcApellido == undefined) {
      // this.toasterService.pop('error', 'Error', 'Debe agregar  apellido');
      alert('Debe agregar apellido');

      return;
    } else if (this.personal.iEdad == null || this.personal.iEdad == '' || this.personal.iEdad == undefined) {
      // this.toasterService.pop('error', 'Error', 'Debe agregar  edad');
      alert('Debe agregar edad');

      return;
    }
    return true;

  }

  eliminar(objt, id) {
    for (let i = objt.length - 1; i >= 0; i--) {
      if (objt[i].vcCodigo == id) {
        objt.splice(i, 1);
      }
    }
  }

  // modales
  public openModalsm(template: TemplateRef<any>) {
    this.modalRef = this.modal.open(template, { size: 'sm' });
  }

  private cerrar(resultado): void {
    this.modalRef.close(resultado);
  }

  public descartar(): void {
    this.modalRef.dismiss();
  }


  // pagination
  actulizarPage(paciente: any, itemPages: number) {
    this.activePage = 1;
    this.fvIndex = 1;
    this.nofvPaginators = 10;
    this.fvPaginator = 0;
    this.lvIndex = this.itemsPpg;
    this.lvPaginator = this.nofvPaginators;

    this.paginators = [];
    if (paciente.length % itemPages === 0) {
      this.nufPaginators = Math.floor(paciente.length / itemPages);
    } else {
      this.nufPaginators = Math.floor(paciente.length / itemPages + 1);
    }
    for (let i = 1; i <= this.nufPaginators; i++) {
      this.paginators.push(i);
    }
  }

  changePage(event: any) {
    if (event.target.text >= 1 && event.target.text <= this.nufPaginators) {
      this.activePage = +event.target.text;
      this.fvIndex = this.activePage * this.itemsPpg - this.itemsPpg + 1;
      this.lvIndex = this.activePage * this.itemsPpg;
    }
  }

  nextPage() {
    if (this.activePage != this.paginators.length && this.paginators.length != 0) {
      if (this.pages.last.nativeElement.classList.contains('active')) {
        if ((this.nufPaginators - this.nofvPaginators) >= this.lvPaginator) {
          this.fvPaginator += this.nofvPaginators;
          this.lvPaginator += this.nofvPaginators;
        } else {
          this.fvPaginator += this.nofvPaginators;
          this.lvPaginator = this.nufPaginators;
        }
      }

      this.activePage += 1;
      this.fvIndex = this.activePage * this.itemsPpg - this.itemsPpg + 1;
      this.lvIndex = this.activePage * this.itemsPpg;
    }

  }

  previousPage() {
    if (this.activePage >= 2) {
      if (this.pages.first.nativeElement.classList.contains('active')) {
        if ((this.lvPaginator - this.fvPaginator) === this.nofvPaginators) {
          this.fvPaginator -= this.nofvPaginators;
          this.lvPaginator -= this.nofvPaginators;
        } else {
          this.fvPaginator -= this.nofvPaginators;
          this.lvPaginator -= (this.nufPaginators % this.nofvPaginators);
        }
      }
      this.activePage -= 1;
      this.fvIndex = this.activePage * this.itemsPpg - this.itemsPpg + 1;
      this.lvIndex = this.activePage * this.itemsPpg;
    }


  }

  firstPage() {
    this.activePage = 1;
    this.fvIndex = this.activePage * this.itemsPpg - this.itemsPpg + 1;
    this.lvIndex = this.activePage * this.itemsPpg;
    this.fvPaginator = 0;
    this.lvPaginator = this.nofvPaginators;
  }

  lastPage() {
    this.activePage = this.nufPaginators;
    this.fvIndex = this.activePage * this.itemsPpg - this.itemsPpg + 1;
    this.lvIndex = this.activePage * this.itemsPpg;

    if (this.nufPaginators % this.nofvPaginators === 0) {
      this.fvPaginator = this.nufPaginators - this.nofvPaginators;
      this.lvPaginator = this.nufPaginators;
    } else {
      this.lvPaginator = this.nufPaginators;
      this.fvPaginator = this.lvPaginator - (this.nufPaginators % this.nofvPaginators);
    }
  }

}

