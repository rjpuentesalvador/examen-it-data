import { UsuarioService } from './servicio/usuario.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { HomeComponent } from './home/home.component';
import { ContacComponent } from './contac/contac.component';
import { GaleryComponent } from './galery/galery.component';
import { ProducComponent } from './produc/produc.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContacComponent,
    GaleryComponent,
    ProducComponent,
    NavMenuComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    ToasterModule,
    HttpClientModule,
    HttpModule,
  ],
  providers: [ToasterService, UsuarioService],
  bootstrap: [AppComponent]
})
export class AppModule { }
