interface ITPersonal {
    id?: number;
    vcCodigo: number;
    vcNombre: string;
    vcApellido: string;
    iEdad: number | string;
}
