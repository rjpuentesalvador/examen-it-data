import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
// import 'rxjs/add/operator/toPromise';
// import { Observable } from 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    private http: Http,
    private httpClient: HttpClient
  ) { }
  public getHeadersClient(): HttpHeaders {
    // let token = sessionStorage.getItem('jwt');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      // 'Authorization': 'Bearer ',
      'Access-Control-Allow-Origin': '*'

    });
    return headers;
  }
  public noAuthHeaders(): Headers {
    return new Headers({
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*'

    });
  }

  public listaUserHttp(): Promise<any> {
    return this.http.get('http://localhost:54606/api/User/GetAllUsers',
      { headers: this.noAuthHeaders(), params: {} }).toPromise().then((data) => {
        return data.json();
      }).catch((error) => {
        throw error;
      });
  }

  public listaUserHttpClient(): Observable<ITPersonal[]> {
    // const json = JSON.stringify(BOM);
    // const encode = encodeURIComponent(json)
    // const postdata = 'format=json&record=' + encode;
    return this.httpClient.get<ITPersonal[]>('http://localhost:54606/api/User/GetAllUsers', { headers: this.getHeadersClient() });
    // return this.httpClient.get('http://localhost:54606/api/User/GetAllUsers');


  }
  // listaUserHttpClient() {
  //   this.httpClient.get('http://localhost:54606/api/User/GetAllUsers').subscribe(data => {
  //     return data;
  //   });
  // }

}
